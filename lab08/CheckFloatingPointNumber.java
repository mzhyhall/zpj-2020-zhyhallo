import java.util.regex.Pattern;

public class CheckFloatingPointNumber {

    private static String correctFloatingPointNumberRegex = "[-+]?[0?|[1-9]+][\\.,]?\\d*";

    public static boolean isFloatingNumber(String number) {
        return Pattern.matches(correctFloatingPointNumberRegex, number);
    }

    public static void main(String[] args) {
        String[] tests = { "123.198878", "12h,111", "-0.00001", "-10", "123", "123.198878", "f.1", "s", "0000", "1.2.3", "0", "-0", "09", "0000.09", "0.88", "-0.00001", "-10", "64-21", "\\2018", "666,6666666" };

        for (String s : tests)
          System.out.print(s + " ");
        System.out.print("\n");

        for (String testCase : tests)
            System.out.printf("Numer %s jest prawidlowa liczba zmiennoprzecinkowa ? %b\n", testCase, isFloatingNumber(testCase));
        
        
    }
}
