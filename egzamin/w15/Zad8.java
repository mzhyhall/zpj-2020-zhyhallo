import java.time.ZoneId;
import java.time.LocalDate;

public class AllTheZonesToday {
  public static void main(String[] args) {
    ZoneId.getAvailableZoneIds().stream()
                                .map(ZoneId::of)
                                .map(LocalDate::now)
                                .sorted()
                                .forEach(System.out::println);
  }
}
