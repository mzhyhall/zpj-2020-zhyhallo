import java.util.regex.*;
import java.util.function.Function;

public class ReplacingGroups {

  public static void main(String[] args) {
    String[] tests = {
      "Ania Shirley, 14, Kobieta, Kanada",
      "Frodo Baggins, 54, Mężczyzna, The Shire",
      "Stanisław Wokulski, 30, Mężczyzna, Polska",
      "Rodion Raskolnikow, 23, Mężczyzna, Rosja"
    };


    for (String desc : tests)
      System.out.println(makeDetailedDescription(desc));

  }

  public static String makeDetailedDescription(String input) {
    Pattern p = Pattern.compile("([A-Z]\\p{IsAlphabetic}+) ([A-Z]\\p{IsAlphabetic}+), ([0-9]+), ([A-Z]\\p{IsAlphabetic}+), ([A-Z]\\p{IsAlphabetic}+([A-Z]\\p{IsAlphabetic}+)*)");
    String newDescription = p.matcher(input).replaceAll( (MatchResult characterData) -> {
        StringBuilder output = new StringBuilder();

        output.append("Imię: " + characterData.group(1) + "\n");
        output.append("Nazwisko: " + characterData.group(2) + "\n");
        output.append("Wiek: " + characterData.group(3) + "\n");
        output.append("Płeć: " + characterData.group(4) + "\n");
        output.append("Kraj: " + characterData.group(5) + "\n");

        return output.toString();
      });

    return newDescription;
  }

}
