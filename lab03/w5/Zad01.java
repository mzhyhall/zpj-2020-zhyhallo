import java.util.*;
import java.util.stream.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.charset.StandardCharsets;

public class Zad01 {
    public static void main(String[] args) throws IOException {
        String fileToRead = args[0];
        List<String> contents = Arrays.asList(new String(Files.readAllBytes(Paths.get(fileToRead)), StandardCharsets.UTF_8).split("\\PL+"));
        Stream<String> words = contents.stream();

        Stream<String> longWords = words.filter(word -> word.length() > 10)
                                        .peek(word -> System.out.println("Method call for " + word))
                                        .limit(5);

        longWords.forEach(System.out::println);


    }
}
