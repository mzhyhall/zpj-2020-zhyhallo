import java.util.*;
import java.util.stream.*;

public class Zad07 {
    public static void main(String[] args) {
        Stream<Integer> infinite = Stream.iterate(-1024, i -> i + 2);
        Stream<Integer> finite = IntStream.range(1, 10).boxed();

        Stream<Integer> zipped = zip(infinite, finite);

        zipped.forEach(System.out::println);

    }

    public static <T> Stream<T> zip(Stream<T> first, Stream<T> second) {
        Spliterator<T> firstStreamPointer = first.spliterator();
        Spliterator<T> secondStreamPointer = second.spliterator();

    int outStreamCharacteristics = firstStreamPointer.characteristics() 
                                   & secondStreamPointer.characteristics()
                                   & ~(Spliterator.DISTINCT | Spliterator.SORTED);

    long sumOfSizes = Long.sum(firstStreamPointer.getExactSizeIfKnown(), secondStreamPointer.getExactSizeIfKnown());
    long outStreamSize = sumOfSizes >= 0 ? sumOfSizes : Long.MAX_VALUE;

    Iterator<T> firstGetter = Spliterators.iterator(firstStreamPointer);
    Iterator<T> secondGetter = Spliterators.iterator(secondStreamPointer);

    Iterator<T> outSupplier = new Iterator<T>() {
        private long counter = 0;

        @Override
        public boolean hasNext() {
            return firstGetter.hasNext() || secondGetter.hasNext();
        }

        @Override
        public T next() {
            T outElement = null;

            if (counter % 2 == 0 && firstGetter.hasNext()) outElement = firstGetter.next();

            else if (counter % 2 != 0 && secondGetter.hasNext()) outElement = secondGetter.next();

            ++counter;
            return outElement;
        }
    };

    Spliterator<T> outSourceSpliterator = Spliterators.spliterator(outSupplier, outStreamSize, outStreamCharacteristics);
    return StreamSupport.stream(outSourceSpliterator, false);
    }
}
