import java.util.*;
import java.util.stream.*;

public class Zad03 {
  public static void main(String[] args) {
    int[] values = { 1, 4, 9, 16, 25 };
    Stream valStream = Stream.of(values); //Strumień int[] zawiera jeden obiekt tablicowy.
    System.out.println(valStream.getClass());

    // Do rozwiązania problemu boxingu i przejścia na typy prymitywne, musimy stworzyć IntStream

    IntStream primitiveStream = Arrays.stream(values); 
    // Żeby strumień składać na bieżąco, można też użyć IntStream.Builder
    System.out.println(primitiveStream.getClass());
  }
}
