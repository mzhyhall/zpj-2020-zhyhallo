package lab01.w1;

public class Main {
    static double average(Measurable[] objects){
        double sum = 0;
        for (Measurable measurable : objects) {
            sum += measurable.getMeasure();
        }
        return sum / objects.length;
    }

    static Measurable largest(Measurable[] objects){
        Measurable largest = objects[0];
        for (Measurable measurable : objects) {
            if (measurable.getMeasure() > largest.getMeasure()){
                largest = measurable;
            }
        }
        return largest;
    }

    public static void main(String[] args) {
        Employee[] employees = new Employee[]{new Employee("dad", 12), new Employee("mom", 15)};

        //Average
        double avg = Main.average((Measurable[])employees);

        //Largest
        Measurable largest = Main.largest((Measurable[])employees);
    }
}