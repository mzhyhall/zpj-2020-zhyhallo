package lab01.w1;

public interface Measurable {
    double getMeasure();
}