import java.time.LocalDate;
import java.time.DayOfWeek;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.Temporal;
import java.util.function.Predicate;

public class NextCorrectDate {
  public static void main(String[] args) {
    LocalDate today = LocalDate.now();
    LocalDate victoryDay = LocalDate.of(1945, 5, 7);
    LocalDate dayInInfamy = LocalDate.of(1941, 12, 7);
    LocalDate justARegularDay = LocalDate.of(2004, 5, 1);

    Predicate<LocalDate> firstDayOfSpring = d ->  d.getMonthValue() == 3 && d.getDayOfMonth() == 21;

    Predicate<LocalDate> firstFridayThe13th = d -> d.getDayOfMonth() == 13 && d.getDayOfWeek() == DayOfWeek.FRIDAY;

    Predicate<LocalDate> first29thOfFebruary = d ->  d.getMonthValue() == 2 && d.getDayOfMonth() == 29;

    System.out.println("Pierwszy dzień wiosny: ");
    System.out.println(today + " ---> " + today.with(następny(firstDayOfSpring)));
    System.out.println(victoryDay + " ---> " + victoryDay.with(następny(firstDayOfSpring)));
    System.out.println(dayInInfamy + " ---> " + dayInInfamy.with(następny(firstDayOfSpring)));
    System.out.println(justARegularDay + " ---> " + justARegularDay.with(następny(firstDayOfSpring)));

    System.out.println("Pierwszy piątek trzynastego: ");
    System.out.println(today + " ---> " + today.with(następny(firstFridayThe13th)));
    System.out.println(victoryDay + " ---> " + victoryDay.with(następny(firstFridayThe13th)));
    System.out.println(dayInInfamy + " ---> " + dayInInfamy.with(następny(firstFridayThe13th)));
    System.out.println(justARegularDay + " ---> " + justARegularDay.with(następny(firstFridayThe13th)));

    System.out.println("Pierwszy luty 29-go: ");
    System.out.println(today + " ---> " + today.with(następny(first29thOfFebruary)));
    System.out.println(victoryDay + " ---> " + victoryDay.with(następny(first29thOfFebruary)));
    System.out.println(dayInInfamy + " ---> " + dayInInfamy.with(następny(first29thOfFebruary)));
    System.out.println(justARegularDay + " ---> " + justARegularDay.with(następny(first29thOfFebruary)));
  }

  public static TemporalAdjuster następny(Predicate<LocalDate> warunek) {
    return (Temporal input) -> {
      LocalDate out = (LocalDate) input;
      do {
        out = (LocalDate) out.plus(1, ChronoUnit.DAYS);
      } while (!warunek.test(out)); 

      return out;
    };
  }
}
