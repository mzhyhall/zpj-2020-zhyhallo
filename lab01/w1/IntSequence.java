package lab01.w1;

import java.util.Iterator;

public class IntSequence {
    static class Sequence implements Iterator<Integer>{
        int[] seq;
        int iter = 0;

        public Sequence(int[] seq){
            this.seq = seq;
        }

        @Override
        public boolean hasNext() {
            return iter < seq.length;
        }

        @Override
        public Integer next() {
            Integer val = seq[iter];
            iter++;
            return val;
        }

    }

    static Iterator<Integer> of(int... args){
        return (Iterator<Integer>)(new Sequence(args));
    }
}