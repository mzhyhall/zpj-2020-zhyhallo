import java.util.*;
import java.util.stream.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.charset.StandardCharsets;

public class Zad02 {
    public static void main(String[] args) throws IOException {
        streamTest(args);
        
        parallelStreamTest(args);
    }
        
    public static void streamTest(String[] args) throws IOException {
        String fileToRead = args[0];
        List<String> contents = Arrays.asList(new String(Files.readAllBytes(Paths.get(fileToRead)), StandardCharsets.UTF_8).split("\\PL+"));


        long before = System.currentTimeMillis();
        long count = contents.stream()
                             .filter(word -> word.length() > 10)
                             .count();
        long after = System.currentTimeMillis();
        System.out.println("Normal: " + Long.toString(after - before));
    }
    
    public static void parallelStreamTest(String[] args) throws IOException {
        String fileToRead = args[0];
        List<String> contents = Arrays.asList(new String(Files.readAllBytes(Paths.get(fileToRead)), StandardCharsets.UTF_8).split("\\PL+"));


        long before = System.currentTimeMillis();
        long count = contents.parallelStream()
                             .filter(word -> word.length() > 10)
                             .count();
        long after = System.currentTimeMillis();
        System.out.println("Parallel: " + Long.toString(after - before));
    }

}
