package lab02.w4;

import java.io.File;
import java.util.ArrayList;

public class Main {

    //Zadanie 1
    static void fileSort(ArrayList<File> files){
        files.sort((f1, f2)->{
            if(f1.isDirectory() && !f2.isDirectory()){
                return 1;
            }else if(!f1.isDirectory() && f2.isDirectory()){
                return -1;
            }

            return f1.getName().compareTo(f2.getName());
        });
    }
    
    //Zadanie 2
    static Runnable combineRunnable(Runnable... runnables){
        return () -> {
            for (Runnable runnable : runnables) {
                runnable.run();
            }
        };
    }
}