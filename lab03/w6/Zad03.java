import java.util.*;
import java.util.function.Function;
import java.util.stream.*;
import java.io.*;
import java.nio.file.*;

public class Zad03 {
    private static final String vowels = "aeyiou";
    public static void main(String[] args) {
        String fileToRead = args[0];

        try (BufferedReader in = Files.newBufferedReader(Paths.get(fileToRead))) {
            
            Stream<String> tokens = in.lines().map(w -> w.toLowerCase());
            // Sugeruję dla 8 samogłosek by ograniczyć ilość słów na wyjściu.
            tokens.filter(w -> countDistinctVowels(w) == 5).forEach(System.out::println);
            
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public static int countDistinctVowels(String word) {

        return (int) word.chars()
                            .mapToObj(c -> (char) c)
                            .distinct()
                            .filter(Zad03::isVowel)
                            .count();
    }

    private static boolean isVowel(char potentialVowel) {
        return vowels.indexOf(potentialVowel) != -1;
    }
}
