import java.util.regex.*;

public class ReplaceThreeLetterWords {
  private static String regex = "\\bc[a-z][a-z]\\b";


  public static void main(String[] args) {
    String test = "cat camera can pen cow cab cot";

    System.out.println(replacer(test));
  }

  public static String replacer(String input) {
    Pattern p = Pattern.compile(regex);
    Matcher m = p.matcher(input);

    while (m.find()) {
      String toReplace = m.group();
      input = input.replace(toReplace, toReplace.toUpperCase());
    }

    return input;
  }
}
