import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class DaysSpentRottingOnThisPlanet {
  public static void main(String[] args) {
    LocalDate myBirth = LocalDate.of(1994, 7, 17);
    LocalDate today = LocalDate.now();

    System.out.println(myBirth.until(today, ChronoUnit.DAYS));
  }
}
