package lab02.w3;

import java.io.Console;

public class Greeter implements Runnable{
    String target;
    Integer count;

    public Greeter(String target, Integer count){
        this.target = target;
        this.count = count;
    }

    @Override
    public void run() {
        for (int i = 0; i < this.count; i++) {
            System.out.print("Witaj, " + this.target);
        }
    }
}