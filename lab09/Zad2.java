import java.time.LocalDate;
import java.time.Year;

public class AddOneYear {
  public static void main(String[] args) {
    LocalDate before = LocalDate.of(2000, 2, 29);
    LocalDate end = before.plusYears(1);

    System.out.println(before);
    System.out.println(end);

  }
}
