package lab01.w2;

import java.util.Iterator;

public class IntSequence {
    public static Iterator<Integer> constant(Integer val){
        return new Iterator<Integer>(){
			@Override
			public boolean hasNext() {
				return true;
			}

			@Override
			public Integer next() {
				return val;
			}
        };
    }
}