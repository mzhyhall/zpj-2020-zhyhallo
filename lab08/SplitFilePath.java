import java.util.regex.*;

public class SplitFilePath {
  public static void main(String[] args) {
    String[] tests = {
        "/media/magazyn/Dokumenty/kameleon for foobar/foobar-kameleon-1.2.2/Plugins/Milkdrop2/data/blur_vs.fx",
        "/media/magazyn/Dokumenty/kameleon for foobar/foobar-kameleon-1.2.2/Plugins/Milkdrop2/data/comp_ps.fx",
        "/media/magazyn/Dokumenty/kameleon for foobar/foobar-kameleon-1.2.2/Plugins/Milkdrop2/data/comp_vs.fx",
        "/media/magazyn/Dokumenty/kameleon for foobar/foobar-kameleon-1.2.2/Plugins/Milkdrop2/data/include.fx",
        "/media/magazyn/Dokumenty/kameleon for foobar/foobar-kameleon-1.2.2/Plugins/Milkdrop2/data/vms_desktop.dll",
        "/media/magazyn/Dokumenty/kameleon for foobar/foobar-kameleon-1.2.2/Plugins/Milkdrop2/data/warp_ps.fx",
        "/media/magazyn/Dokumenty/kameleon for foobar/foobar-kameleon-1.2.2/Plugins/Milkdrop2/data/warp_vs.fx",
        "/media/magazyn/Dokumenty/kameleon for foobar/foobar-kameleon-1.2.2/Plugins/Milkdrop2/presets/Geiss - Blur Mix 3.milk",
        "/media/magazyn/Dokumenty/kameleon for foobar/foobar-kameleon-1.2.2/Plugins/ml_pmp.dll",
        "/media/magazyn/Dokumenty/kameleon for foobar/foobar-kameleon-1.2.2/Plugins/nscrt.dll",
        "/media/magazyn/Dokumenty/kameleon for foobar/foobar-kameleon-1.2.2/Plugins/out_ds.dll",
        "/media/magazyn/Dokumenty/kameleon for foobar/foobar-kameleon-1.2.2/Plugins/vis_milk2.dll",
        "/media/magazyn/Dokumenty/kameleon for foobar/foobar-kameleon-1.2.2/winamp.exe"
    };

    for (String path : tests)
      printElements(path);
  }

  public static void printElements(String input) {
    String[] elements = Pattern.compile("/").splitAsStream(input)
                                            .filter(w -> w.length() > 0)
                                            .toArray(String[]::new);

    System.out.println("Katalogi:");
    for (int i = 0; i < elements.length - 1; ++i) {
      System.out.println(elements[i]);
    }

    String fileName = elements[elements.length - 1].split("\\.")[0];

    System.out.println("\nNazwa pliku:\n" + fileName);

    Matcher extension = Pattern.compile("(.+?)\\.(\\w+)$").matcher(input);
    extension.find();

    System.out.println("\nRozszerzenie:");
    System.out.println(extension.group(2) + "\n");
  }
}
