import java.util.*;
import java.util.stream.*;

public class Zad01 {
    public static void main(String[] args) {
        
        String[] testCasesLetter = { "!@#@#$^%&*", "zapytanie?", "XENON", "isWord??", "123456", "אונברסיטה", "ასბიოე", "шємьол҄ь" };
        String[] testCasesIdentifiers = {"mcDn", "BeQgnCSo[B", "EZrQWc", "J=lt}", "t\"r}F5UjdK", "KIGmX", "BMxCQ)LF", "r;UM", ")agLvr",
                "yELXzspl", "bDNjSAmHl", "sJ3upe", "eSjWU>F", ", ", "cHtS", "4AP", "[kWr#", "jr]WORBUP", "FcFhNFLL", "eeYrswVyc",
                "d&rD`!Z,{T", "zel>tfmS", "DW'T)HuhEG", "XV;Tstr", "0VSkrtdK", "tDgXbc@zRm", "Blzi", "s-KMJHF", "ptfSi,", "goto" };

        for (String word : testCasesLetter)
          System.out.println(word + " is only made of letters: " + isMadeOfLettersOnly(word));

        for (String word : testCasesIdentifiers)
          System.out.println(word + " is a valid Java identifier: " + isValidJavaIdentifier(word));
    }
    
    public static boolean isMadeOfLettersOnly(String word) {
        return word.chars()
                   .mapToObj(c -> (char) c)
                   .allMatch(Character::isAlphabetic);
    }

    static final String[] javaKeywords = { "abstract",  "continue", "for", "new", "switch",
        "assert", "default", "goto", "package", "synchronized", "boolean", "do", "if", "private", "this",
        "break", "double", "implements", "protected", "throw", "byte", "else", "import", "public", "throws",
        "case", "enum", "instanceof", "return", "transient", "catch", "extends", "int", "short", "try",
        "char", "final", "interface", "static", "void", "class", "finally", "long", "strictfp", "volatile",
        "const", "float", "native", "super", "while" };

    public static boolean isValidJavaIdentifier(String word) {
        if (Arrays.stream(javaKeywords).anyMatch(word::equals);) return false; // If is java key word
            
        char firstCharacter = word.charAt(0);

        if (!Character.isJavaIdentifierStart(firstCharacter))
            return false;

        boolean areTheRestOfTheLettersFine = word.chars()
                                                 .skip(1) // pierwszy znak juz sprawdzony
                                                 .mapToObj(c -> (char) c)
                                                 .allMatch(c -> Character.isJavaIdentifierPart(c));

        return areTheRestOfTheLettersFine;
    }
}
