import java.time.ZoneId;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class FunWithFlights {
  public static void main(String[] args) {
    LocalDate startDay = LocalDate.now();
    LocalTime firstFlightStartingTime = LocalTime.of(15, 5);
    LocalTime secondFlightStartingTime = LocalTime.of(14, 5);
    LocalTime secondFlightLandingTime = LocalTime.of(16, 5);
    ZoneId germanTimeZone = ZoneId.of("Europe/Berlin");
    ZoneId californianTimeZone = ZoneId.of("America/Los_Angeles");

    
    ZonedDateTime departureFromLosAngeles = ZonedDateTime.of(startDay, firstFlightStartingTime, californianTimeZone);
    ZonedDateTime arrivalInFrankfurt = departureFromLosAngeles.plusMinutes(10 * 60 + 50).withZoneSameInstant(germanTimeZone);

    System.out.println("Dotrzesz do Frankfurtu: " + arrivalInFrankfurt.toLocalDateTime()); 

    
    ZonedDateTime departureFromFrankfurt = ZonedDateTime.of(startDay, secondFlightStartingTime, germanTimeZone);
    
    ZonedDateTime arrivalInLosAngeles = ZonedDateTime.of(startDay, secondFlightLandingTime, californianTimeZone).withZoneSameInstant(germanTimeZone); 
    long timeOfSecondFlight = departureFromFrankfurt.until(arrivalInLosAngeles, ChronoUnit.MINUTES);

    System.out.println("Drugi lot trwał: " + (timeOfSecondFlight / 60) + " godzin i " + (timeOfSecondFlight % 60) + " minut.");
  }
}
