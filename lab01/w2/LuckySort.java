package lab01.w2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class LuckySort {
    private static <T extends Comparable<? super T>>
            boolean isSorted(Iterable<T> iterable, Comparator<T> comp) {
        Iterator<T> iter = iterable.iterator();
        if (!iter.hasNext()) {
            return true;
        }
        T t = iter.next();
        while (iter.hasNext()) {
            T t2 = iter.next();
            if ( comp.compare(t, t2) > 0) {
                return false;
            }
            t = t2;
        }
        return true;
    }

    static void sort(ArrayList<String> strings, Comparator<String> comp){
        while(!isSorted(strings, comp)){
            Collections.shuffle(strings);
        }
    }
}