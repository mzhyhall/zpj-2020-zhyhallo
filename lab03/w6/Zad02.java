import java.util.*;
import java.util.function.Function;
import java.util.stream.*;
import java.io.*;
import java.nio.file.*;

public class Zad02 {
    public static void main(String[] args) {
        String fileToRead = args[0];

        try (BufferedReader in = Files.newBufferedReader(Paths.get(fileToRead))) {
            Stream<String> tokens = in.lines()
                                        .map(w -> w.split("\\PL+"))
                                        .flatMap(arr -> Arrays.stream(arr))
                                        .limit(100);

            tokens.forEach(System.out::println);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        try (BufferedReader in = Files.newBufferedReader(Paths.get(fileToRead))) {

        Stream<String> tokens = in.lines()
                                .map(w -> w.split("\\PL+"))
                                .flatMap(arr -> Arrays.stream(arr))
                                .map(w -> w.toLowerCase())
                                .filter(w -> w.length() > 2)
                                .sorted(Comparator.naturalOrder());

        TreeMap<String, Integer> wordCounts = tokens.collect(Collectors.toMap(
                                                                     Function.identity(), 
                                                                     word -> 1,
                                                                     Integer::sum,
                                                                     TreeMap::new
                                                                     ));

        wordCounts.entrySet().stream()
                           .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                           .limit(10)
                           .forEach(System.out::println);

        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
