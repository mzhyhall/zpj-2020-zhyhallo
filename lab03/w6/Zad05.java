import java.util.function.Function;
import java.util.*;
import java.util.stream.*;
import java.io.*;
import java.nio.file.*;

public class Zad05 {
    public static void main(String[] args) {
        String fileToRead = "test_text.txt";

        try (BufferedReader in = Files.newBufferedReader(Paths.get(fileToRead))) {

            Collection<String> tokens = in.lines()
                                                .map(s -> s.split("\\PL+"))
                                                .flatMap(a -> Arrays.stream(a))
                                                .filter(w -> w.length() != 0)
                                                .collect(Collectors.toCollection(ArrayList::new));

            Stream<String> longestTokens = getLongestStrings(tokens);
            longestTokens.forEach(System.out::println);

        } catch (IOException excpt) {
            excpt.printStackTrace();
        }
  }

    public static Stream<String> getLongestStrings(Collection<String> strings) {
        int length = strings.stream()
                                    .map(String::length)
                                    .max(Comparator.naturalOrder())
                                    .orElse(0);
        Stream<String> longest = strings.stream()
                                    .filter(w -> w.length() == length);

        return longest;
    }
}
