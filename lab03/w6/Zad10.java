import java.util.*;
import java.util.stream.*;
import java.math.BigInteger;

public class Zad10 {
    public static void main(String[] args) {
        largePrimes();
        largePrimesParallel();
    }
    
    public static void largePrimes() {
        Random enthropy = new Random();

        final int MINIMUM_BITS = 163;
        // chance for test probability to let not prime number into list is equal to 2 ^ -PROBABILITY
        final int PROBABILITY = 999999;


        Stream<BigInteger> makeMeSomePrimes = Stream.generate(() -> new BigInteger(MINIMUM_BITS + 1, enthropy))
                                                                .filter(i -> i.bitLength() > MINIMUM_BITS)
                                                                .filter(i -> i.isProbablePrime(PROBABILITY))
                                                                .limit(500);
        ArrayList<BigInteger> myPrimes = new ArrayList<BigInteger>(500); 

        long before = System.currentTimeMillis();
        makeMeSomePrimes.collect(Collectors.toCollection(() -> myPrimes));
        long after = System.currentTimeMillis();

        //~ myPrimes.stream().forEach(System.out::println);

        System.out.println("Normal: "+Long.toString(after - before));
    }

    public static void largePrimesParallel() {
        Random enthropy = new Random();

        final int MINIMUM_BITS = 163;
        // chance for test probability to let not prime number into list is equal to 2 ^ -PROBABILITY
        final int PROBABILITY = 999999;


        Stream<BigInteger> makeMeSomePrimes = Stream.generate(() -> new BigInteger(MINIMUM_BITS + 1, enthropy))
                                                                .parallel()
                                                                .filter(i -> i.bitLength() > MINIMUM_BITS)
                                                                .filter(i -> i.isProbablePrime(PROBABILITY))
                                                                .limit(500);
        ArrayList<BigInteger> myPrimes = new ArrayList<BigInteger>(500);

        long before = System.currentTimeMillis();
        makeMeSomePrimes.collect(Collectors.toCollection(() -> myPrimes));
        long after = System.currentTimeMillis();

        //~ myPrimes.stream().forEach(System.out::println);
        System.out.println(myPrimes.get(0).toString());

        System.out.println("Parellel: "+Long.toString(after - before));
    }
}
