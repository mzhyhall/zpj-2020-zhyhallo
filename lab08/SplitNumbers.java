import java.util.regex.*;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class SplitNumbers {

  private static String correctNumberRegex = "[-+]?\\d+";

  public static ArrayList<Integer> withFind(String input) {
    Pattern p = Pattern.compile(correctNumberRegex);
    Matcher m = p.matcher(input);

    ArrayList<Integer> numbers = new ArrayList<>();

    while (m.find()) {
      numbers.add(Integer.parseInt(m.group()));
    }
    return numbers;
  }

  public static ArrayList<Integer> withSplit(String input) {
    Pattern p = Pattern.compile("[^\\d+-]|[-+][^\\d]+");

    ArrayList<Integer> numbers = p.splitAsStream(input)
                                  .filter(w -> w.length() > 0)
                                  .map(Integer::parseInt)
                                  .collect(Collectors.toCollection(ArrayList::new));
    return numbers;
  }

  public static void main(String[] args) {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    System.out.println("Metoda find: ");
    br.lines().forEach(s -> System.out.println(withFind(s)));

    System.out.println("Metoda split: ");
    br.lines().forEach(s -> System.out.println(withSplit(s)));
    
  }
}
