package lab02.w4;

public class Employee{
    public String name;
    public double salary;

    public Employee(String name, double salary){
        this.name = name;
        this.salary = salary;
    }
}