import java.util.regex.Pattern;

public class CheckHouseNumber {

    private static String correctHouseNumber = "[0-9]+[A-Za-z0-9]*\\\\[0-9]+[A-Za-z0-9]*";

    public static boolean isHouseNumber(String number) {
        return Pattern.matches(correctHouseNumber, number);
    }

    public static void main(String[] args) {
        String[] tests = { "123\\2A", "24B\\3", "12\\5", "abc\\cba", "666ABCD\\8hwdp", "00-22234JKLll" };

        for (String testCase : tests)
            System.out.printf("Numer %s jest prawidlowym numerem domu? %b\n", testCase, isHouseNumber(testCase));
        
        
    }
}
