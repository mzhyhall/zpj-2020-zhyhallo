import java.util.*;
import java.util.stream.*;

public class Zad05 {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<String>();
        words.add("Kochajmy");
        words.add("płaskoziemców");
        words.add("za");
        words.add("dostarczanie");
        words.add("dobrego");
        words.add("humoru");
        words.add(","); 
        words.add("tak"); 
        words.add("szybko");
        words.add("wyginą");

        for (String word : words) {
            letters(word).forEach(letter -> System.out.print(letter + " "));
            System.out.println();
        }
        System.out.println();
    }

  public static Stream<String> letters(String word) {
    return word.chars().mapToObj(c -> (char) c).map(letter -> Character.toString(letter));

  }

}
