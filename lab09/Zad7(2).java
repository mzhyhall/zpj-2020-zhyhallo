import java.time.LocalTime;

public class TimeInterval {
  private LocalTime startingTime;
  private LocalTime endingTime;

  public TimeInterval(int startHour, int startMinutes, int endingHour, int endingMinutes) {
    startingTime = LocalTime.of(startHour, startMinutes);
    endingTime = LocalTime.of(endingHour, endingMinutes);
  }

  public TimeInterval(LocalTime start, LocalTime end) {
    startingTime = start;
    endingTime = end;
  }
  
  public boolean isOverlapping(TimeInterval other) {
    return other.startingTime.compareTo(this.endingTime) < 0 &&
      other.endingTime.compareTo(this.startingTime) > 0;
  }

  @Override
  public String toString() {
    return "Początek: " + startingTime + " Koniec: " + endingTime;
  }
}
