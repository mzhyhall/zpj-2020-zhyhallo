import java.time.LocalDate;
import java.time.Month;
import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.Locale;

public class UnixCal {
  public static void main(String[] args) {
    Month month = Month.of(Integer.valueOf(args[0]));
    int year = Integer.valueOf(args[1]);

    StringBuilder buffer = new StringBuilder();

    buffer.append(month.getDisplayName(TextStyle.FULL_STANDALONE, Locale.getDefault()) + " " + year + "\n");
    buffer.append("po wt śr cz pi so ni\n");

    LocalDate firstDayOfMonth = LocalDate.of(year, month, 1);
    int firstWeekPad = firstDayOfMonth.getDayOfWeek().getValue() - 1; 

    for (int i = 0; i < firstWeekPad; ++i)
      buffer.append("   ");

    for (int i = 1; i <= firstDayOfMonth.lengthOfMonth(); ++i) {
      if (i < 10) buffer.append(" ");

      buffer.append(i + " ");

      if ((i + firstWeekPad) % 7 == 0) // test na niedziele
        buffer.append("\n");
    }

    System.out.println(buffer.toString());
  }
}
