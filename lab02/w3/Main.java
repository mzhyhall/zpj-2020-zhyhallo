package lab02.w3;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.ArrayList;

public class Main {
    static void greeders() throws InterruptedException{
        var gr1 = new Greeter("Kot", 12);
        var gr2 = new Greeter("Adam", 12);

        var t1  = new Thread(gr1);
        var t2  = new Thread(gr2);

        t1.start();
        t2.start();

        t1.wait();
        t2.wait();
    }

    static void runTogether(Runnable... tasks){
        for (Runnable runnable : tasks) {
            new Thread(runnable).start();
        }
    }

    static void runInOrder(Runnable... tasks){
        for (Runnable runnable : tasks) {
            runnable.run();
        }
    }

    static ArrayList<File> listDirs(String rootPath, FileFilter filter){
        ArrayList<File> output = new ArrayList<File>();
        listDirsRec(rootPath, output, filter);
        return output;
    }

    static void listDirsRec(String dirPath, ArrayList<File> output, FileFilter filter){
        File dir = new File(dirPath);
        File[] files = dir.listFiles(filter);
        for (File file : files) {
            if(file.isDirectory()){
                output.add(file);
                listDirsRec(file.toPath().toString(), output, filter);
            }
        }
    }

    static String[] listFiles(File directory, String extension){
        return directory.list((file, name)->name.endsWith(extension));
    }

    public static void main(String[] args) throws InterruptedException {
        greeders();
        listDirs("/home/slamy/Documents/Docs", (file) -> true);
        listFiles(new File("/home/slamy/Documents/Docs"), "txt");
    }
}