import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.ArrayList;
import java.util.stream.Stream;

public class GetThreeLetterStringsStartingWithR {

    private static Pattern myLittleRegex = Pattern.compile("r[a-z][a-z]");

    public static ArrayList<String> findMyWords(String input) {
        Matcher matcher = myLittleRegex.matcher(input);
        ArrayList<String> outStrings = new ArrayList<>();

        while (matcher.find())
            outStrings.add(matcher.group());

        return outStrings;

    }

    public static void main(String[] args) {
        String test = "I saw a terrible rat running through the forest.";

        findMyWords(test).stream().forEach(System.out::println);
        
    }
}
