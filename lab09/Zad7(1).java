public class TestTimeInterval {
  public static void main(String[] args) {
   TimeInterval a = new TimeInterval(10, 0, 12, 30); 
   TimeInterval b = new TimeInterval(11, 15, 15, 0); 
   TimeInterval c = new TimeInterval(18, 0, 23, 30); 
   TimeInterval d = new TimeInterval(13, 0, 18, 0); 

   System.out.println(a + "\n" + b + "\nNakladaja sie ? " + a.isOverlapping(b) + "\n");
   System.out.println(b + "\n" + a + "\nNakladaja sie ? " + b.isOverlapping(a) + "\n");
   System.out.println(a + "\n" + c + "\nNakladaja sie ? " + a.isOverlapping(c) + "\n");
   System.out.println(a + "\n" + d + "\nNakladaja sie ? " + a.isOverlapping(d) + "\n");
   System.out.println(d + "\n" + c + "\nNakladaja sie ? " + d.isOverlapping(c) + "\n");
   System.out.println(c + "\n" + b + "\nNakladaja sie ? " + c.isOverlapping(b) + "\n");
   System.out.println(c + "\n" + a + "\nNakladaja sie ? " + c.isOverlapping(a) + "\n");
   System.out.println(d + "\n" + b + "\nNakladaja sie ? " + d.isOverlapping(b) + "\n");
  }
}
