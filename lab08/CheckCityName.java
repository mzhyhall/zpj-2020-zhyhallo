import java.util.regex.Pattern;

public class CheckCityName {

    private static String correctCityName = "[A-Z]{1}[a-z]+([ -]{1}[A-Z][a-z]+){0,1}";

    public static boolean isCityName(String number) {
        return Pattern.matches(correctCityName, number);
    }

    public static void main(String[] args) {
        String[] tests = { "Czestochowa", "Jelenia Gora", "Nuremeberg", "Myszkow123", "Lubie jesc rybe", "Bielsko-Biala" };

        for (String testCase : tests)
            System.out.printf("%s jest prawidlowa nazwa miasta? %b\n", testCase, isCityName(testCase));
        
        
    }
}
