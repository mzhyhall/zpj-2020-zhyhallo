import java.time.LocalDate;
import java.time.DayOfWeek;

// bardziej sprytne rozwiazanie problemu
// https://wwwhome.ewi.utwente.nl/~ptdeboer/misc/friday13.html

public class JasonFavouriteDays {
  public static void main(String[] args) {
    LocalDate beginningOfCentury = LocalDate.of(1901, 1, 1);

    LocalDate toCheck = beginningOfCentury.plusDays(12);

    while (toCheck.getYear() < 2001) {
      if (toCheck.getDayOfWeek() == DayOfWeek.FRIDAY)
        System.out.println(toCheck);
      toCheck = toCheck.plusMonths(1);
    }
    
  }
}
