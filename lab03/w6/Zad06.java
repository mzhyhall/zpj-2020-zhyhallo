import java.util.*;
import java.util.stream.*;

public class Zad06 {
    public static void main(String[] args) {
        Stream<Integer> finiteStream = IntStream.range(1, 666).boxed();
        Stream<Long> secFiniteStream = LongStream.range(Long.MIN_VALUE, Long.MAX_VALUE).boxed();
        Stream<Integer> infiniteStream = Stream.iterate(2, i -> i + i);

        System.out.println("Is finite finite ? " + isFinite(finiteStream));

        //Tu pomimo że strumień jest skończony, jego rozmiar przekracza tolerowany przez funkcję. 
        System.out.println("Is secFinite finite ? " + isFinite(secFiniteStream));

        System.out.println("Is infinite finite ? " + isFinite(infiniteStream));
    }
    
    // To nie jest dobry pomysł ponieważ:
    // Strumienie moga pochodzic z modyfikowalnych po wywołaniu kolekcji
    // Strumienie z funkcji/generatorow mogą się nigdy nie kończyć


    // Metoda skutecznie ocenia 'nieskończoność strumienia' tylko dla strumieni krótszych niż MAX_VALUE dla klasy Long
    public static <T> boolean isFinite(Stream<T> stream) {
        return stream.spliterator().estimateSize() < Long.MAX_VALUE;
    }
}
