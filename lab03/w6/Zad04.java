import java.util.*;
import java.util.stream.*;
import java.io.*;
import java.nio.file.*;

public class Zad04 {
    public static void main(String[] args) {
        String fileToRead = "test_text.txt";

        try (BufferedReader in = Files.newBufferedReader(Paths.get(fileToRead))) {

            Stream<String> tokens = in.lines()
                                            .map(s -> s.split("\\PL+"))
                                            .flatMap(a -> Arrays.stream(a))
                                            .filter(w -> w.length() != 0);

            System.out.println("Average length is: " + getAverageLength(tokens));

        } catch (IOException excpt) {
          excpt.printStackTrace();
        }
    }

    public static double getAverageLength(Stream<String> strings) {
        return strings.collect(Collectors.summarizingInt(w -> w.length())).getAverage();
    }
}
