import java.util.*;
import java.util.stream.*;

public class Zad08 {
    public static void main(String[] args) {
        List<ArrayList<Integer>> vectors = new ArrayList<ArrayList<Integer>>();
        int[] numberCounts = { 4, 8, 10, 3, 12, 6, 15, 4, 1, 7 };

        Random generator = new Random();

        for (int i = 0; i < numberCounts.length; ++i) {
            System.out.println("Generate vector nr " + (i + 1));
            vectors.add(new ArrayList<Integer>());
            fillWithNumbers(vectors.get(i), numberCounts[i], generator);
            System.out.println(vectors.get(i));
        }

        ArrayList<Integer> oneArrayListToCollectThemAll = vectors.stream()
                                                             .reduce((first, second) -> joinTwoArrayLists(first, second))
                                                             .get();

        oneArrayListToCollectThemAll = vectors.stream()
                                          .reduce(new ArrayList<Integer>(), 
                                                  (first, second) -> joinTwoArrayLists(first, second));

        oneArrayListToCollectThemAll = vectors.parallelStream()
                                          .reduce(new ArrayList<Integer>(),
                                                  (first, second) -> Stream.concat(first.stream(), second.stream())
                                                                           .collect(Collectors.toCollection(ArrayList::new)),
                                                  (first, second) -> joinTwoArrayLists(first, second));

                                                  
        oneArrayListToCollectThemAll.stream().forEach(System.out::println);
  }

    public static void fillWithNumbers(ArrayList<Integer> collection, int howManyToGenerate, Random gen) {
        for (int i = 0; i < howManyToGenerate; ++i) collection.add(gen.nextInt());
    }

    public static <T> ArrayList<T> joinTwoArrayLists(ArrayList<T> first, ArrayList<T> second) {
        first.addAll(second);
        return first;
    }
}
