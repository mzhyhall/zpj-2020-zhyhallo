import java.time.LocalDate;
import java.time.Year;

public class NoPlusDaysAllowed {
  public static void main(String[] args) {
    LocalDate leap = LocalDate.ofYearDay(2004, 256);
    LocalDate current = LocalDate.ofYearDay(Year.now().getValue(), 256);

    System.out.println(leap);
    System.out.println(current);

  }
}
