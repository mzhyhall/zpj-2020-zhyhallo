import java.util.*;
import java.util.stream.*;

public class Zad04 {
    public static void main(String[] args) {
        Stream<Long> testGenerator = randInt(0, 25214903917L, 11, 248); 
        System.out.println("Example: ");
        testGenerator.limit(20).forEach(System.out::println);
        
        
        if (args.length != 4) {
              System.out.println("Start with 4 arguments into program: seed, a, c i m.");
              System.exit(1);
        }
        long seed = Integer.valueOf(args[0]);
        int a = Integer.valueOf(args[1]);
        int c = Integer.valueOf(args[2]);
        long m = Integer.valueOf(args[3]);
        
        Stream<Long> myGenerator = randInt(seed, a, c, m);
        System.out.println("My numbers: ");
        myGenerator.limit(20).forEach(System.out::println);

    }

    public static Stream<Long> randInt(long seed, long multiplier, int increment, long modulo) {
        return Stream.iterate(seed, start -> (multiplier * start + increment) % modulo);
    }

}
