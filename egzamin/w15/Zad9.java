import java.time.ZoneOffset;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class NotFullHourOffsets {
  public static void main(String[] args) {
    ZoneId.getAvailableZoneIds().stream()
                                    .map(ZoneId::of)
                                    .map(ZonedDateTime::now)
                                    .filter(x -> x.getOffset().getTotalSeconds() % 3600 != 0)
                                    .forEach(x -> System.out.println(x.getZone() + " " + x.getOffset()));

  }
}
